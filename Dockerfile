FROM tensorflow/tensorflow:1.13.1-gpu-py3-jupyter

COPY requirements.txt requirements.txt

RUN pip3 install -r requirements.txt

