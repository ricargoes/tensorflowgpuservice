# TensorflowGPU service

Dockerized python tensorflow with CUDAs, able to connect with host GPU. A jupyter notebook is exposed to use the host remotely.

Functionality can be extended adding python modules (as keras) to the requirements.txt file
